"use strict";

import { bootstrap } from 'react-fastgui';
import backend from 'react-fastgui-backend-html';
import demoPage from './demo';

bootstrap(backend, demoPage("Press Cmd+R (Mac), Ctrl+R (Windows) to reload"));
