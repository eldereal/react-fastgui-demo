"use strict";

var path = require('path');
var webpack = require('webpack');
var ImageScaleGroupLoader = require("image-scale-group-loader");

var NODE_ENV = process.env.NODE_ENV;

var env = {
  production: NODE_ENV === 'production',
  staging: NODE_ENV === 'staging',
  test: NODE_ENV === 'test',
  development: NODE_ENV === 'development' || typeof NODE_ENV === 'undefined'
};

Object.assign(env, {
  build: (env.production || env.staging)
});

var transformUuid = "transform-react-jsx-source";
try {
    require('babel-preset-react-native/transforms/transform-react-fastgui-jsx-uuid');
    transformUuid = 'babel-preset-react-native/transforms/transform-react-fastgui-jsx-uuid';
} catch(e) {
    console.warn("cannote load transform-react-fastgui-jsx-uuid, use transform-react-jsx-source instead");
}

var babelrc = {
  "plugins": [
      "transform-runtime",
      transformUuid,
      "transform-react-jsx"
  ],
  "presets": ["es2015"]
};

module.exports = {
  target: 'web',

  entry: [
    'babel-polyfill',
    './client/main.jsx'
  ],

  output: {
    path: path.join(__dirname, '/client'),
    pathInfo: true,
    publicPath: 'http://localhost:3000/client/',
    filename: 'main.js'
  },

  resolve: {
    root: path.join(__dirname, ''),
    modulesDirectories: [
      'node_modules'
    ],
    extensions: ['', '.webpack.js', '.web.js', '.js', '.jsx']
  },

  plugins: [
    new webpack.DefinePlugin({
      __DEV__: env.development,
      __STAGING__: env.staging,
      __PRODUCTION__: env.production,
      __CURRENT_ENV__: '\'' + (NODE_ENV) + '\''
  }),
  new webpack.ResolverPlugin([ImageScaleGroupLoader], ["normal"])
  ],

  module: {
    loaders: [
      // { test: require.resolve("react-fastgui"), loader: "babel", query: babelrc },
      // { test: require.resolve("react-fastgui-backend-html"), loader: "babel", query: babelrc },
      { test: /\.css$/, loader: 'style!css!autoprefixer?browsers=last 2 version' },
      { test: /\.scss$/, loader: 'style!css!autoprefixer?browsers=last 2 version!sass?outputStyle=expanded' },
      { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader?limit=10000&minetype=application/font-woff" },
      { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader" },
      { test: /\.jsx?$/, loader: 'babel', query: babelrc, exclude: /node_modules/ },
      {
        test: /\.jsx?$/,
        loader: 'babel',
        query: babelrc,
        include: [
          path.join(__dirname, "..", "node_modules", "react-fastgui-backend-html"),
          path.join(__dirname, "..", "node_modules", "react-fastgui"),
        ]
      },
    ],

    noParse: /\.min\.js/
  }
};
