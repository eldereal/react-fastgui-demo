"use strict";

import React, { bootstrap, Label, Group, Button, TextBox } from 'react-fastgui';

const styles = {
    flex:{
        flex: 1
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        paddingLeft: 40,
        paddingRight: 40,
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5
    },
    content: {
        textAlign: 'justify',
        color: '#333333',
        marginBottom: 5
    },
    border: {
        padding: 10,
        borderWidth: 0.5,
        borderColor: "#333333"
    },
    row: {
        flexDirection: "row",
        alignItems: 'center',
    },
    centering: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    elem: {
        borderWidth: 0,
        borderRadius: 5,
        backgroundColor: "#f0f0f0",
        height: 22,
        flexDirection: "row",
        alignItems: 'center'
    },
    elemText: {
        fontSize: 14
    },
    indent: {
        marginLeft: 12
    },
    comment: {
        color: "#226E24"
    }
};

export default function(platformDescriptions){
    let clickCount = 0;
    let text = "<TextBox value={ text }/>";
    function* demo(){
        yield <Group style={styles.container}>
            <Group>
                <Label style={styles.welcome}>
                    Welcome to React Fastgui!
                </Label>
                <Label style={styles.instructions}>
                    To get started, edit index.ios.js
                </Label>
                <Label style={styles.instructions}>
                    { platformDescriptions }
                </Label>
            </Group>
            <Group style={{marginTop: 20}}>
            {function*(){
                yield <Label style={ styles.welcome }>Fastgui is simple and intuitive</Label>;
                yield <Label style={ styles.content }>
                    You can use generator functions as React components.
                    And get result of user interactions without events.
                </Label>;
                yield <Label style={ styles.content }>
                    For example:
                </Label>;
                yield <Group style={ styles.border }>
                    <Group style={ styles.row }>
                        if (yield
                        {function*(){
                            if(yield <Button style={ styles.elem }><Label style={ styles.elemText }>{'<Button>Click here</Button>'}</Label></Button>){
                                clickCount ++;
                            }
                        }}
                        { ') {' }
                    </Group>

                    <Label style={ styles.indent }> clickCount ++;  </Label>
                    {function*(){
                        yield <Label style={[ styles.indent, styles.comment ]}> {'//'} clickCount: { clickCount } </Label>;
                    }}


                    <Label>}</Label>
                    <Group style={ styles.row }>
                        text = yield
                        {function*(){
                            text = yield <TextBox style={[ styles.flex, styles.elem, styles.elemText ]} value={ text }/>;
                        }}
                        ;
                    </Group>
                    {function*(){
                        yield <Label style={[ styles.comment ]}> {'//'} text: { text } </Label>;
                    }}
                </Group>;
            }}</Group>
        </Group>;
    }
    demo.displayName = "demo";
    return demo;
}
