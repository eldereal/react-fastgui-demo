"use strict";
//
// const extend = require('babel-runtime/helpers/extends').default;
//
// function processProps(props, overrideProps, nonOverrideProps) {
//     if (!props) {
//         return props;
//     }
//     let override;
//     if (Array.isArray(props.style)) {
//         if (!override) {
//             override = {};
//         }
//         override.style = extend({}, ...props.style);
//     }
//     const style = override && override.style || props.style;
//     if (style) {
//         for (const dir of['Top', 'Right', 'Bottom', 'Left', '']) {
//             if (style[`border${dir}Width`] && !style.borderStyle) { //html needs borderStyle to display borders
//                 if (!override) {
//                     override = {};
//                 }
//                 if (!override.style) {
//                     override.style = extend({}, style);
//                 }
//                 override.style[`border${dir}Style`] = 'solid'; //default solid style
//             }
//         }
//     }
//
//     if (nonOverrideProps) {
//         for (const key in nonOverrideProps) {
//             if (overrideProps && overrideProps[key]) {
//                 continue; //this prop is already processed in above for-loop
//             }
//             const original = override && override[key] || props[key];
//             if (typeof(nonOverrideProps[key]) === "object" && typeof(original) === "object") {
//                 if (!override) {
//                     override = {};
//                 }
//                 override[key] = extend({}, nonOverrideProps[key], original);
//             } else if (original === undefined) {
//                 if (!override) {
//                     override = {};
//                 }
//                 override[key] = nonOverrideProps[key];
//             }
//         }
//     }
//
//     if (overrideProps) {
//         for (const key in overrideProps) {
//             if (!override) {
//                 override = {};
//             }
//             const original = override[key] || props[key];
//             // console.info("key", key, "original", original);
//             if (overrideProps[key] !== null && typeof(overrideProps[key]) === "object") {
//                 const orin = typeof(original) === "object" && original || {};
//                 const nonOverride = nonOverrideProps && nonOverrideProps[key] || {};
//                 // console.info("extend", nonOverride, orin, overrideProps[key]);
//                 override[key] = extend({}, nonOverride, orin, overrideProps[key]);
//             } else {
//                 override[key] = overrideProps[key];
//             }
//         }
//     }
//
//     if (override) {
//         return extend({}, props, override);
//     } else {
//         return props;
//     }
// }
// console.info(processProps({
//     "source": {
//         "src": "http://localhost:3000/client/slogan.png",
//         "width": 198,
//         "height": 41,
//         "bytes": 1909,
//         "type": "png"
//     },
//     "style": [{
//         "width": 99,
//         "height": 20.5
//     }, {
//         hello: 1
//     }],
//     "__source": {
//         "fileName": "/Users/elm/Playground/react-fastgui-demo-ios/lib/routes/dashboard.js",
//         "lineNumber": 122
//     }
// }, {
//     "source": null,
//     "style": {
//         "background": "url(http://localhost:3000/client/slogan.png) no-repeat",
//         "backgroundSize": "contain"
//     }
// }, {
//     "style": {
//         "width": 198,
//         "height": 41
//     }
// }));
// module.exports = processProps;
//


var obj = {};
obj[Symbol.iterator] = function(){
    return {
        next: function(){
            return { value: 1, done: false };
        }
    };
};



for(var i of obj){
    console.info(i);
    break;
}
