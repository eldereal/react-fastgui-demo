"use strict";

import { bootstrap } from 'react-fastgui';
import backend from 'react-fastgui-backend-ios';
import demoPage from './demo';

bootstrap(backend, demoPage("Press Cmd+R to reload,\nCmd+D or shake for dev menu"), { appName: "ReactFastguiDemo" });
